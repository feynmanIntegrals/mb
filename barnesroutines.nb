(* Content-type: application/mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 6.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       145,          7]
NotebookDataLength[     10757,        356]
NotebookOptionsPosition[      8589,        278]
NotebookOutlinePosition[      9541,        312]
CellTagsIndexPosition[      9337,        304]
WindowFrame->Normal
ContainsDynamic->False*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell[BoxData[
 RowBox[{"<<", "MB.m"}]], "Input",
 CellChangeTimes->{{3.442126241136169*^9, 3.442126242081168*^9}}],

Cell[CellGroupData[{

Cell[BoxData["\<\"MB 1.2\"\>"], "Print",
 CellChangeTimes->{3.442126243576084*^9}],

Cell[BoxData["\<\"by Michal Czakon\"\>"], "Print",
 CellChangeTimes->{3.442126243578457*^9}],

Cell[BoxData["\<\"improvements by Alexander Smirnov\"\>"], "Print",
 CellChangeTimes->{3.442126243582305*^9}],

Cell[BoxData["\<\"more info in hep-ph/0511200\"\>"], "Print",
 CellChangeTimes->{3.442126243585369*^9}],

Cell[BoxData["\<\"last modified 2 Jan 09\"\>"], "Print",
 CellChangeTimes->{3.442126243588348*^9}]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"<<", "barnesroutines.m"}]], "Input",
 CellChangeTimes->{{3.442126245152605*^9, 3.4421262543511257`*^9}}],

Cell[BoxData["\<\"Barnes Routines, v 1.0.0 of November 1, 2007\"\>"], "Print",
 CellChangeTimes->{3.442126251185107*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"?", "DoAllBarnes"}]], "Input",
 CellChangeTimes->{{3.4421265563373613`*^9, 3.442126558694929*^9}}],

Cell[BoxData[
 StyleBox["\<\"DoAllBarnes[intlist_List,progressive_:False,bound_:1] \
repeatedly applies the Barnes lemmas to a list of integrals.  It first \
applies the second Barnes lemma, then the first Barnes lemma.  If the second \
argument is True, the lemmas are progressively applied to integrals of \
decreasing dimension starting with the highest-dimensional ones.  The last \
argument is passed to DoBarnes1 and DoBarnes2.\"\>", "MSG"]], "Print", \
"PrintUsage",
 CellChangeTimes->{3.442126559484591*^9},
 CellTags->"Info3442130159-5528637"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"?", "DoBarnes1"}]], "Input",
 CellChangeTimes->{{3.442126575917646*^9, 3.442126578330482*^9}}],

Cell[BoxData[
 StyleBox["\<\"DoBarnes1[int_MBint,bound_:1] applies the first Barnes lemma \
to the given MB integral.  It first looks for a linear transformation of the \
integration variables which will make such an application possible. The \
optional argument specifies a bound on the integer coefficients in such a \
transformation.\"\>", "MSG"]], "Print", "PrintUsage",
 CellChangeTimes->{3.442126578618319*^9},
 CellTags->"Info3442130178-9325522"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"?", "DoBarnes2"}]], "Input",
 CellChangeTimes->{{3.4421265792978888`*^9, 3.442126581823722*^9}}],

Cell[BoxData[
 StyleBox["\<\"DoBarnes2[int_MBint,bound_:1] applies the second Barnes lemma \
to the given MB integral.  It first looks for a linear transformation of the \
integration variables which will make such an application possible. The \
optional argument specifies a bound on the integer coefficients in such a \
transformation.\"\>", "MSG"]], "Print", "PrintUsage",
 CellChangeTimes->{3.442126582152546*^9},
 CellTags->"Info3442130182-8008696"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"?", "SetVerbosity"}]], "Input",
 CellChangeTimes->{{3.442126583396388*^9, 3.442126630805521*^9}}],

Cell[BoxData[
 StyleBox["\<\"SetVerbosity[n_Integer] sets the reporting level for Barnes \
routines. Levels run from 0 to 3, and routines report more information at \
higher levels.\"\>", "MSG"]], "Print", "PrintUsage",
 CellChangeTimes->{3.442126631384678*^9},
 CellTags->"Info3442130231-2098329"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"int", "=", 
  RowBox[{"{", 
   RowBox[{"MBint", "[", 
    RowBox[{
     RowBox[{
      RowBox[{"(", 
       RowBox[{"1", "/", 
        RowBox[{"Gamma", "[", 
         RowBox[{"2", "+", "z2"}], "]"}]}], ")"}], " ", 
      RowBox[{"Gamma", "[", 
       RowBox[{
        RowBox[{"-", "1"}], "-", "z2"}], "]"}], " ", 
      RowBox[{
       RowBox[{"Gamma", "[", 
        RowBox[{"1", "+", "z2"}], "]"}], "^", "2"}], " ", 
      RowBox[{"Gamma", "[", 
       RowBox[{
        RowBox[{"-", "1"}], "-", "z4"}], "]"}], " ", 
      RowBox[{"Gamma", "[", 
       RowBox[{
        RowBox[{"-", "2"}], "-", "z2", "-", "z4"}], "]"}], " ", 
      RowBox[{"Gamma", "[", 
       RowBox[{"2", "+", "z4"}], "]"}], " ", 
      RowBox[{"Gamma", "[", 
       RowBox[{"2", "+", "z2", "+", "z4"}], "]"}]}], ",", 
     RowBox[{"{", 
      RowBox[{
       RowBox[{"{", 
        RowBox[{"ep", "\[Rule]", "0"}], "}"}], ",", 
       RowBox[{"{", 
        RowBox[{
         RowBox[{"z2", "\[Rule]", 
          RowBox[{"-", 
           RowBox[{"(", 
            RowBox[{"1", "/", "3"}], ")"}]}]}], ",", 
         RowBox[{"z4", "\[Rule]", 
          RowBox[{"-", 
           RowBox[{"(", 
            RowBox[{"4", "/", "3"}], ")"}]}]}]}], "}"}]}], "}"}]}], "]"}], 
   "}"}]}]], "Input",
 CellChangeTimes->{{3.4421272382817183`*^9, 3.442127239618704*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"MBint", "[", 
   RowBox[{
    RowBox[{
     FractionBox["1", 
      RowBox[{"Gamma", "[", 
       RowBox[{"2", "+", "z2"}], "]"}]], 
     RowBox[{
      RowBox[{"Gamma", "[", 
       RowBox[{
        RowBox[{"-", "1"}], "-", "z2"}], "]"}], " ", 
      SuperscriptBox[
       RowBox[{"Gamma", "[", 
        RowBox[{"1", "+", "z2"}], "]"}], "2"], " ", 
      RowBox[{"Gamma", "[", 
       RowBox[{
        RowBox[{"-", "1"}], "-", "z4"}], "]"}], " ", 
      RowBox[{"Gamma", "[", 
       RowBox[{
        RowBox[{"-", "2"}], "-", "z2", "-", "z4"}], "]"}], " ", 
      RowBox[{"Gamma", "[", 
       RowBox[{"2", "+", "z4"}], "]"}], " ", 
      RowBox[{"Gamma", "[", 
       RowBox[{"2", "+", "z2", "+", "z4"}], "]"}]}]}], ",", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{"{", 
       RowBox[{"ep", "\[Rule]", "0"}], "}"}], ",", 
      RowBox[{"{", 
       RowBox[{
        RowBox[{"z2", "\[Rule]", 
         RowBox[{"-", 
          FractionBox["1", "3"]}]}], ",", 
        RowBox[{"z4", "\[Rule]", 
         RowBox[{"-", 
          FractionBox["4", "3"]}]}]}], "}"}]}], "}"}]}], "]"}], 
  "}"}]], "Output",
 CellChangeTimes->{3.442127240047002*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"DoAllBarnes", "[", 
  RowBox[{"int", ",", "True"}], "]"}]], "Input",
 CellChangeTimes->{{3.442127241582695*^9, 3.442127245940495*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"\<\"1 x 2\"\>", ",", "\<\"0 x 1\"\>", ",", "\<\"0 x 0\"\>"}], 
  "}"}]], "Print",
 CellChangeTimes->{
  3.442127246294279*^9, {3.442127283538823*^9, 3.442127295997695*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"\<\"2 x 1\"\>", ",", "\<\"0 x 0\"\>"}], "}"}]], "Print",
 CellChangeTimes->{
  3.442127246294279*^9, {3.442127283538823*^9, 3.442127296224744*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"\<\"1 x 1\"\>", ",", "\<\"1 x 0\"\>"}], "}"}]], "Print",
 CellChangeTimes->{
  3.442127246294279*^9, {3.442127283538823*^9, 3.442127297409837*^9}}]
}, Open  ]],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"MBint", "[", 
    RowBox[{
     RowBox[{"EulerGamma", " ", 
      RowBox[{"PolyGamma", "[", 
       RowBox[{"2", ",", "1"}], "]"}]}], ",", 
     RowBox[{"{", 
      RowBox[{
       RowBox[{"{", 
        RowBox[{"ep", "\[Rule]", "0"}], "}"}], ",", 
       RowBox[{"{", "}"}]}], "}"}]}], "]"}], ",", 
   RowBox[{"MBint", "[", 
    RowBox[{
     FractionBox[
      RowBox[{
       RowBox[{"Gamma", "[", 
        RowBox[{
         RowBox[{"-", "1"}], "-", "z2"}], "]"}], " ", 
       RowBox[{"Gamma", "[", 
        RowBox[{"-", "z2"}], "]"}], " ", 
       SuperscriptBox[
        RowBox[{"Gamma", "[", 
         RowBox[{"1", "+", "z2"}], "]"}], "3"], " ", 
       RowBox[{"PolyGamma", "[", 
        RowBox[{"0", ",", 
         RowBox[{"1", "+", "z2"}]}], "]"}]}], 
      RowBox[{"Gamma", "[", 
       RowBox[{"2", "+", "z2"}], "]"}]], ",", 
     RowBox[{"{", 
      RowBox[{
       RowBox[{"{", 
        RowBox[{"ep", "\[Rule]", "0"}], "}"}], ",", 
       RowBox[{"{", 
        RowBox[{"z2", "\[Rule]", 
         RowBox[{"-", 
          FractionBox["1", "3"]}]}], "}"}]}], "}"}]}], "]"}]}], 
  "}"}]], "Output",
 CellChangeTimes->{
  3.442127247824563*^9, {3.442127285580551*^9, 3.4421272974341*^9}}]
}, Open  ]]
},
WindowSize->{640, 750},
WindowMargins->{{150, Automatic}, {Automatic, 52}},
ShowSelection->True,
FrontEndVersion->"6.0 for Linux x86 (64-bit) (June 2, 2008)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{
 "Info3442130159-5528637"->{
  Cell[1687, 62, 552, 9, 119, "Print",
   CellTags->"Info3442130159-5528637"]},
 "Info3442130178-9325522"->{
  Cell[2397, 80, 453, 7, 100, "Print",
   CellTags->"Info3442130178-9325522"]},
 "Info3442130182-8008696"->{
  Cell[3010, 96, 454, 7, 100, "Print",
   CellTags->"Info3442130182-8008696"]},
 "Info3442130231-2098329"->{
  Cell[3625, 112, 298, 5, 62, "Print",
   CellTags->"Info3442130231-2098329"]}
 }
*)
(*CellTagsIndex
CellTagsIndex->{
 {"Info3442130159-5528637", 8907, 289},
 {"Info3442130178-9325522", 9016, 292},
 {"Info3442130182-8008696", 9125, 295},
 {"Info3442130231-2098329", 9234, 298}
 }
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[590, 23, 114, 2, 28, "Input"],
Cell[CellGroupData[{
Cell[729, 29, 82, 1, 21, "Print"],
Cell[814, 32, 92, 1, 21, "Print"],
Cell[909, 35, 109, 1, 21, "Print"],
Cell[1021, 38, 103, 1, 21, "Print"],
Cell[1127, 41, 98, 1, 21, "Print"]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[1274, 48, 128, 2, 28, "Input"],
Cell[1405, 52, 120, 1, 21, "Print"]
}, Open  ]],
Cell[CellGroupData[{
Cell[1562, 58, 122, 2, 28, "Input"],
Cell[1687, 62, 552, 9, 119, "Print",
 CellTags->"Info3442130159-5528637"]
}, Open  ]],
Cell[CellGroupData[{
Cell[2276, 76, 118, 2, 28, "Input"],
Cell[2397, 80, 453, 7, 100, "Print",
 CellTags->"Info3442130178-9325522"]
}, Open  ]],
Cell[CellGroupData[{
Cell[2887, 92, 120, 2, 28, "Input"],
Cell[3010, 96, 454, 7, 100, "Print",
 CellTags->"Info3442130182-8008696"]
}, Open  ]],
Cell[CellGroupData[{
Cell[3501, 108, 121, 2, 28, "Input"],
Cell[3625, 112, 298, 5, 62, "Print",
 CellTags->"Info3442130231-2098329"]
}, Open  ]],
Cell[CellGroupData[{
Cell[3960, 122, 1347, 41, 80, "Input"],
Cell[5310, 165, 1188, 38, 105, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[6535, 208, 158, 3, 28, "Input"],
Cell[CellGroupData[{
Cell[6718, 215, 212, 5, 21, "Print"],
Cell[6933, 222, 187, 4, 21, "Print"],
Cell[7123, 228, 187, 4, 21, "Print"]
}, Open  ]],
Cell[7325, 235, 1248, 40, 116, "Output"]
}, Open  ]]
}
]
*)

(* End of internal cache information *)

